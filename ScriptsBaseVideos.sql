CREATE DATABASE  IF NOT EXISTS `BASEVIDEOS` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `BASEVIDEOS`;
-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: BASEVIDEOS
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ListasReproduccion`
--

DROP TABLE IF EXISTS `ListasReproduccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ListasReproduccion` (
  `nombre` varchar(50) DEFAULT NULL,
  `tema` varchar(45) DEFAULT NULL,
  `fechacreacion` varchar(45) DEFAULT NULL,
  `calificacion` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ListasReproduccion`
--

LOCK TABLES `ListasReproduccion` WRITE;
/*!40000 ALTER TABLE `ListasReproduccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ListasReproduccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsuariosAdministrativos`
--

DROP TABLE IF EXISTS `UsuariosAdministrativos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UsuariosAdministrativos` (
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `edad` int DEFAULT NULL,
  `id` varchar(45) NOT NULL,
  `correoElectronico` varchar(45) DEFAULT NULL,
  `nombreUsuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `canton` varchar(45) DEFAULT NULL,
  `distrito` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsuariosAdministrativos`
--

LOCK TABLES `UsuariosAdministrativos` WRITE;
/*!40000 ALTER TABLE `UsuariosAdministrativos` DISABLE KEYS */;
INSERT INTO `UsuariosAdministrativos` VALUES ('juan','perez',20,'1234567','correo@correo','hola','hola12345','admin','h','h','h');
/*!40000 ALTER TABLE `UsuariosAdministrativos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsuariosFinales`
--

DROP TABLE IF EXISTS `UsuariosFinales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UsuariosFinales` (
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `edad` int DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `correoElectronico` varchar(45) DEFAULT NULL,
  `nombreUsuario` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `canton` varchar(45) DEFAULT NULL,
  `distrito` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`nombre`,`contrasena`,`nombreUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsuariosFinales`
--

LOCK TABLES `UsuariosFinales` WRITE;
/*!40000 ALTER TABLE `UsuariosFinales` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsuariosFinales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Videos`
--

DROP TABLE IF EXISTS `Videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Videos` (
  `nombre` varchar(50) DEFAULT NULL,
  `fechasubido` varchar(45) DEFAULT NULL,
  `calificacion` int DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Videos`
--

LOCK TABLES `Videos` WRITE;
/*!40000 ALTER TABLE `Videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-03 22:08:15
