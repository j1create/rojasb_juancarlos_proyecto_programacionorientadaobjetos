package rojasjuan;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import rojas.juan.bl.usuario.Usuario;
import rojas.juan.login.ValidacionLogin;
import rojas.juan.tl.ControllerUAdmin;
import rojas.juan.tl.ControllerUFinal;

import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;


public class ControllerLogIn {

    public CheckBox toggleAdministrativo;
    private static ControllerUAdmin tmpControllerUAdmin= new ControllerUAdmin();
    private static ControllerUFinal tmpControllerUFinal = new ControllerUFinal();
    private static ValidacionLogin tmpvalidacion = new ValidacionLogin();
    private static Usuario usuarioLoggeado = new Usuario();

    @FXML
    private AnchorPane LoginScene;

    @FXML
    private PasswordField LogIn_Contrasena;

    @FXML
    private TextField LogIn_NombreUsuario;

    @FXML
    private ImageView FotoPerfil;

    @FXML
    private Button InicioSesion;

    @FXML
    private Hyperlink OlvidoContrasena;

    @FXML
    private Button CrearCuenta;

    @FXML
    private AnchorPane FormularioCuenta;

    @FXML
    private TextField Nombre;

    @FXML
    private TextField Identificacion;

    @FXML
    private TextField Edad;

    @FXML
    private TextField CorreoElectronico;

    @FXML
    private TextField Contrasena;

    @FXML
    private Button CrearCuentaNueva;

    @FXML
    private TextField NombreUsuario;

    @FXML
    private TextField Apellido;

    @FXML
    private TextField inputProvincia;

    @FXML
    private TextField inputCanton;

    @FXML
    private TextField inputDistrito;

    @FXML
    private Pane avisoFinalCuenta;

    @FXML
    private Button volverInicio;

    @FXML
    private Pane errorEnDatos;

    @FXML
    private Button volverIntentar;

    @FXML
    private CheckBox isAdministrador;
    

    @FXML
    void CrearCuenta(MouseEvent event) {
    }

    public void IniciarSesion(javafx.scene.input.MouseEvent mouseEvent) {
        if(isAdministrador.isSelected()){
            try{
                if (LogIn_NombreUsuario.getText() != null && LogIn_Contrasena.getText() != null){
                    boolean siExiste = tmpvalidacion.loginUsuarioAdmin(LogIn_NombreUsuario.getText(), LogIn_Contrasena.getText());
                    if (siExiste ==true){
                        //validar usuario: listo
                        //cargar usuario: listo
                        //cargar videos: en proceso
                        //cargar listas de reproduccion: en proceso
                        usuarioLoggeado = tmpvalidacion.cargarUsuarioAdmin(LogIn_NombreUsuario.getText(), LogIn_Contrasena.getText());
                        System.out.println("Usuario loggeado");
                    } else {
                        System.out.println("Usuario no encontrado");
                    }
                }
            } catch (Exception e){
                System.out.println("Error en conexion a BD");
            }
        } else{
            try{
                if (LogIn_NombreUsuario.getText() != null && LogIn_Contrasena.getText() != null){
                    boolean siExiste = tmpvalidacion.loginUsuarioFinal(LogIn_NombreUsuario.getText(), LogIn_Contrasena.getText());
                    if (siExiste ==true){
                        //validar usuario: listo
                        //cargar usuario: listo
                        //cargar videos: en proceso
                        //cargar listas de reproduccion: en proceso
                        usuarioLoggeado = tmpvalidacion.cargarUsuarioFinal(LogIn_NombreUsuario.getText(), LogIn_Contrasena.getText());
                        System.out.println("Usuario loggeado");
                    } else {
                        System.out.println("Usuario no encontrado");
                    }
                }
            } catch (Exception e){
                System.out.println("Error en conexion a BD");
            }
        }
    }

    public void AbrirFormulario(javafx.scene.input.MouseEvent mouseEvent) {
        FormularioCuenta.setVisible(true);
    }

    public void ContrasenaTemporal(javafx.scene.input.MouseEvent mouseEvent) {
    }

    public void CrearCuenta(javafx.scene.input.MouseEvent mouseEvent) {
        try{
            if (Nombre.getText() != null && Apellido.getText() != null && Identificacion.getText() != null
                    && Edad.getText() != null && CorreoElectronico.getText() != null && NombreUsuario.getText() != null
                    && Contrasena.getText() != null && Contrasena.getText().length()>6 && inputProvincia.getText() != null
                    && inputCanton.getText() != null && inputDistrito.getText() != null ){
                if(toggleAdministrativo.isSelected()){
                    tmpControllerUAdmin.insertarUAdministrativo(Nombre.getText(), Apellido.getText(),
                            Integer.parseInt(Edad.getText()), Identificacion.getText(), CorreoElectronico.getText(),
                            NombreUsuario.getText(), Contrasena.getText(), inputProvincia.getText(), inputCanton.getText(),
                            inputDistrito.getText());
                }else{
                    tmpControllerUFinal.insertarUFinal(Nombre.getText(), Apellido.getText(),
                            Integer.parseInt(Edad.getText()), Identificacion.getText(), CorreoElectronico.getText(),
                            NombreUsuario.getText(), Contrasena.getText(), inputProvincia.getText(), inputCanton.getText(),
                            inputDistrito.getText());
                }

            }
        } catch (Exception e){
            // Implementar método para manejar error
            System.out.println("error en info");
        }
        // Implementar metodo para enviar correo de Apache commons
        avisoFinalCuenta.setVisible(true);
    }

    public void regresarInicio(javafx.scene.input.MouseEvent mouseEvent) {
        FormularioCuenta.setVisible(false);
    }


    public void volveraFormulario(javafx.scene.input.MouseEvent mouseEvent) {
    }

    public void perfilAdmin(javafx.scene.input.MouseEvent mouseEvent) {
    }
}
