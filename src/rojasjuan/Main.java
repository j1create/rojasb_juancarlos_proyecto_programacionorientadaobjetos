package rojasjuan;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
Proyecto final: Programacion orientada a objetos.
Estudiante: Juan Carlos Rojas Benavides.
Profesor: Alvaro Cordero Pena.
Version de java implementada: 8.0.251
Conector de base de datos: mysql 8.0.20
Version de proyecto segun fecha: 03/05/2020

Comentarios: se logró hacer las interfaces y manejo de eventos. En cuando a funcionalidades se logran registrar
 usuarios exitosamente en BD y validar el loggin, proceso que requirió de muchísima investigacion teniendo que solucionar
 problemas que tenían que ver principalmente con la conexion a la base de dato a la hora de validar el nombre de usuario
 y contrasena ingresados por el usuario para el logIn. Funcionalidades pendientes: cargar videos y listas de reproduccion,
 sin embargo la lógica del código va en buen camino.
 En la ventana principal queda habilitado el espacio de mediaPlayer para videos y botones para modificacion de preferencias.
 Se realiza la investigacion respectiva sobre cómo cargar el url de los videos utilizando el path absoluto del archivo, sin
 embargo su implementacion queda para la próxima versión.
 */

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("LogIn.fxml"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
